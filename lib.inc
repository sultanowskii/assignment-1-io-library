%define SYS_READ  0
%define SYS_WRITE 1
%define SYS_EXIT  60

%define STDIN  0
%define STDOUT 1
%define STDERR 2

section .text 
 
; Принимает код возврата и завершает текущий процесс
exit:
    ; exit(arg)
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx

    .loop:
        lea rsi, [rdi+rcx]
        mov al, [rsi]
        ; сравнение с нуль-терминатором
        test al, al
        jz .end
        inc rcx
        jmp .loop

    .end:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rsp+0: str
print_string:
    sub rsp, 8

    mov [rsp+0], rdi

    ; len = string_length(arg)
    call string_length
    mov rdx, rax

    mov rsi, [rsp+0]
    mov rdi, STDOUT

    ; write(stdout, arg, len)
    mov rax, SYS_WRITE
    syscall

    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA).
; Напрямую зависит от print_char, порядок важен.
print_newline:
    mov dil, `\n`
; Принимает код символа и выводит его в stdout
; rsp+0: sym
print_char:
    push rdi

    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1

    ; write(stdout, arg, 1)
    mov rax, SYS_WRITE
    syscall

    pop rdi
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rax: n
; rsp+0: s_ptr
; rsp+8: 24-bytes buffer
print_uint:
    sub rsp, 32

    ; ставим указатель на конец буфера
    lea rax, [rsp+31]
    mov [rsp+0], rax

    mov rax, rdi

    ; нуль-терминатор в конце
    mov r11, [rsp+0]
    mov byte[r11], 0

    .loop:
        ; digit = n % 10; n /= 10
        mov r10, 10
        xor rdx, rdx
        div r10

        add dl, '0'

        ; s_ptr--
        dec qword[rsp+0]

        ; *s_ptr = digit
        mov r11, [rsp+0]
        mov byte[r11], dl

        test rax, rax
        jnz .loop
    
    mov rdi, [rsp+0]
    call print_string

    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print

    .handle_negative:
    push rdi

    mov dil, '-'
    call print_char

    pop rdi

    neg rdi

    .print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rsp+0: str1
; rsp+8: str2
string_equals:
    sub rsp, 16
    mov [rsp+0], rdi
    mov [rsp+8], rsi

    .loop:
        mov rdx, [rsp+0]
        mov al, [rdx]
        mov rdx, [rsp+8]

        ; если символы не равны - вовзращаем 0
        cmp al, [rdx]
        jne .not_equal

        ; если символы равны и один из них равен
        ; нуль-терминатору, то это явно конец обоих строк
        test al, al
        jz .equal

        inc qword[rsp+0]
        inc qword[rsp+8]
        jmp .loop

    .equal:
    mov rax, 1
    jmp .end
    
    .not_equal:
    xor rax, rax

    .end:
    add rsp, 16
    ret 

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; rsp+0: sym
read_char:
    sub rsp, 8

    ; n = read(STDIN, &sym, 1)
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_READ
    syscall

    ; if n == 0 => EOF
    test rax, rax
    jz .eof

    .success:
    xor rax, rax
    mov rax, [rsp]
    jmp .end

    .eof:
    xor rax, rax

    .end:
    add rsp, 8
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi: buf
; rsi: n
;
; rsp+0: buf_ptr
; rsp+8: buf_size
; rsp+16: buf
; rsp+24: n
read_word:
    sub rsp, 32

    mov [rsp+0], rdi
    mov qword[rsp+8], 0
    mov [rsp+16], rdi
    mov [rsp+24], rsi

    .trim_loop:
        call read_char

        ; EOF -> failure
        test al, al
        jz .failure

        ; <space> -> skip
        cmp al, `\n`
        jz .trim_loop
        cmp al, ` `
        jz .trim_loop
        cmp al, `\t`
        jz .trim_loop

        ; *buf_ptr = sym
        mov dl, al
        mov rax, [rsp+0]
        mov [rax], dl
        ; buf_ptr++
        inc qword[rsp+0]
        ; cntr++
        inc qword[rsp+8]

    .loop:
        ; cntr == n -> проверяем, что дальше не слово
        mov rax, [rsp+8]
        cmp rax, [rsp+24]
        je .after_last_check

        call read_char

        ; sym == EOF/<space> -> успех
        test al, al
        je .success
        cmp al, `\n`
        je .success
        cmp al, ` `
        je .success
        cmp al, `\t`
        je .success

        ; *buf_ptr = sym
        mov dl, al
        mov rax, [rsp+0]
        mov [rax], dl
        ; buf_ptr++
        inc qword[rsp+0]
        ; cntr++
        inc qword[rsp+8]

        jmp .loop

    .after_last_check:
    call read_char
    ; если `sym` - не часть слова, то успех
    test al, al
    je .success
    cmp al, `\n`
    je .success
    cmp al, ` `
    je .success
    cmp al, `\t`
    je .success

    .failure:
    xor rax, rax
    xor rdx, rdx
    jmp .end

    .success:
    ; нуль-терминатор в конец строки
    mov rax, [rsp+0]
    mov byte[rax], 0

    mov rax, [rsp+16]
    mov rdx, [rsp+8]

    .end:
    add rsp, 32
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rsp+0: s
; rsp+8: num
; rsp+16: i-digit
parse_uint:
    sub rsp, 24

    mov [rsp+0], rdi
    mov qword[rsp+8], 0
    mov qword[rsp+16], 0

    xor rcx, rcx

    .loop:
        ; sym = s[i]
        mov rdi, [rsp+0]
        mov al, [rdi + rcx]

        cmp al, '0'
        jb .end_of_int
        cmp al, '9'
        ja .end_of_int

        ; digit = sym - '0'
        sub al, '0'
        mov [rsp+16], al

        ; num = num * 10 + digit
        mov rax, [rsp+8]
        imul rax, 10
        add rax, [rsp+16]
        mov [rsp+8], rax

        ; i++
        inc rcx
        jmp .loop

    .end_of_int:
    test rcx, rcx
    jz .failure
    
    mov rax, [rsp+8]
    mov rdx, rcx
    jmp .end

    .failure:
    xor rdx, rdx

    .end:
    add rsp, 24
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rsp+0: s
parse_int:
    sub rsp, 8

    mov [rsp+0], rdi

    mov rax, [rsp+0]
    mov al, [rax]

    cmp al, '-'
    je .parse_negative

    .parse_non_negative:
    ; parse_uint(s)
    mov rdi, [rsp+0]
    call parse_uint
    jmp .end

    .parse_negative:
    ; n, status = parse_uint(s + 1)
    mov rdi, [rsp+0]
    inc rdi
    call parse_uint

    ; если status == 0, то парсинг провалился
    test rdx, rdx
    jz .failure

    ; фиксим результат, делая число отрицательным и
    ; увеличивая кол-во символов на 1 (- в начале)
    neg rax
    inc rdx
    jmp .end

    .failure:
    xor rax, rax
    xor rdx, rdx

    .end:
    add rsp, 8
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rsp+0: s
; rsp+8: buf
; rsp+16: buf_size
; rsp+24: s_size
string_copy:
    sub rsp, 32

    mov [rsp+0], rdi
    mov [rsp+8], rsi
    mov [rsp+16], rdx
    mov qword[rsp+24], 0

    call string_length
    cmp rax, [rsp+16]
    ja .failure

    mov [rsp+24], rax

    xor rcx, rcx

    .loop:
        ; sym = s[i]; buf[i] = sym;
        mov rax, [rsp+0]
        mov dl, [rax+rcx]
        mov rax, [rsp+8]
        mov [rax+rcx], dl

        ; i++
        inc rcx

        ; если sym - \0, то успех
        test dl, dl
        jz .success

        jmp .loop

    .failure:
    xor rax, rax
    jmp .end

    .success:
    mov rax, [rsp+24]

    .end:
    add rsp, 32
    ret
